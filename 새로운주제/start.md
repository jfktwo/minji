## 새로운 주제를 찾아보기

## 아는애 주제
한중 한자 비교(근대 현대 소설책 3권해서 거기서 
한국어랑 번역 본을 찾아서 한자를 
뽑아서 똑같은 한자인데 다른뜻으로 쓰이는것)


##  주제의 성격
- 한중비교 이며 언어적인것
- 문화적 차이점에 해당 하는것이 들어있으면 좋다.(언어문화)


## 주제
- 한중비교  
  - 한중 소설설화비교(태평광기의 영향을 중심으로) 1977
  - 한중 고전문학비교
  - 한중 구비,서사문학의 비교
  - 한중일 비교 통사
  - 한중문학 비교연구(따비에서 쟁기가지)
  - 한중문학 비교연구/윤윤진
  - 한중 현대문학 비교
  - 한중 재자가인 소설류 비교연구

- 한중(논문)

  - 한중 미각형용사 달다/쓰다의 디조연구 리위안
   - 한중 미각형용사의 대조연구 / GU CHEN
   - 한중 맛그림씨의 인지의미론적 대조연구 / 료자자    -> 미각단어 관련 한중비교
   - 韓中 촉각형용사 대조연구 / 왕선위 
   - 韓中 外來語 對照 硏究 / 강성애  


   - 한중 신체관련 다의어의 의미 대응 양상 연구   ----(0)

   - 한중 속담의 문화언어학적 비교 고찰 / 동우비

   - 한중 음식 관련 관용표현에 나타난 문화적 차이에 관한 연구
   - 한중 한자어 분류사 통시적 대조 연구
   - 한중 " 푸르다"류 색체어 대조 연구
   - 한중 빈도부사의 대조 연구 / 석전손
   - 한중 비교구문 대조 연구 / 유효단
   
   - 한중일 3국의 개인정보요구 에 관한 사회언어학적 연구 / 김윤희
   
   - 한중 영상광고의 시각적 은유에 관한 연구
   - 한중 경여법 비교 연구
   - 한중 의문문의 반문용법 대조 연구 : 劉歡 / 劉歡
   - 한중 경찰용어 비교연구 / 하은이
  
   - 한중 문화 번역 연구 : 소설 번역을 중심으로 / 마정
   - 한중일 3국의 의뢰행동에 관한 사회언어학적 연구 / 김종완
   - 여성 외모의 아름다움 인식에 대한 한중일 비교문화 연구 / 김선우
   - 한중 국제결혼가정 아동의 중국어 어순 오류에 나타난 한국어 모어 전이 현상
   - 한중어휘번역비교연구 / 담정유
   - 중국의 소프트파워 전략과 한중관계 : 문화를 중심으로 / Wang, Geng
   - 韓中 주요 歲時風俗의 제의적 특성 비교연구 : 문헌자료를 중심으로 / WANG MEIYUN  / 한중,세시풍속
   - 한중일 공통 설화 텍스트를 통한 세시풍속 교육 연구 / 견우직녀를 중심으로 / 김은선 / 
   - 김유정 단편소설의 한중 번역양상 고찰 / 우맹흠
   - 한.중 속담에 나타난 '손'과 '발'의 의미 대조 연구 = 韓中俗語中出現的手和脚的意義對照硏究 / 진, 방  / 
   - 한국 드라마 『쓸쓸하고 찬란하神 - 도깨비』의 한중 자막번역 연구 
   - 한중 신문기사 번역텍스트에 나타난 문제점 : <조선일보> 포털사이트 중문판을 중심으로 / 장림
   - 한중의 호칭어의 대한 대조 연구 : 특히 친척 호칭어를 중심으로 / 왕혜방 
   - 한중 동물 관용어에 대한 인지의미론적 연구 : 12支 동물을 중심으로 / 정아영  
   - 한중 순차통역과 동시통역의 사례 비교 연구 / 전유현 
   - 명사구 구조에 대한 한중대조 연구 = 名词短语韩中文对照研究 / 대, 예선
   - 한중 설날 세시풍속 비교연구 / 왕락  
   - 한중 이동 동사 논항의 대조 연구 / 왕청청  
   - 한중 양태부사 대조 연구 = 韩中语气副词对照硏究 / 김련옥  
   - "了"和"-었-"在韩中翻译中的 使用偏误分析 : 以朝鲜日报中文网的内容为中心 = 한중 번역에서의 '了' 와 '-었-'의 사용 오류 분석 / 김려나  
   - 한중 문화간 의사소통을 위한 문화소 연구 : 중국 TV 드라마『 中国式关系』에 나타난 어휘를 중심으로 / 서예진   
   - 한·중 재귀사의 대조 연구 : 한국어'자기,자신'과 중국어'自己, 自身'을 중심으로 = 韓中反身代詞的對照硏究 : 韓國語‘자기, 자신’和中國語‘自己, 自身’爲中心 / 조석전  
   - 한중 다의 동사의 의미 체계 대조 연구 : 대립어 '앉다'와 '서다'를 중심으로 / 여준요   
   - 한중일 피동문의 대조연구 / 주유강  
   - 번역가의 언어배경에 따른 번역방법의 차이 검증 연구 : 한중문학번역의 감정표현을 중심으로 / 이현주 
   - 한중 비교표현의 대조연구 / 최중식 
   - 신체어 '눈'에 관한 어휘의미론적 韓中대조 / 정미숙 
   - 신조어와 유행어를 통한 한중-중한 의미론적 번역 / 채선 
   - 屍語故事의 유형과 한중일 민담 비교연구 / 김수경 
   - 1인 미디어 게임방송 시청 동기에 대한 한중비교연구 / 왕
   - 韩中日共用汉字808 字字书中字形与 六书分类的偏差研究 = 한중일 공용한자 808자 자서 중 자형과 육서 분류의 편차 연구 / 설원원
   - 한·중 중주어 구문의 대조연구 = 韓中重主語句文的對照硏究 / 한, 금만   
   - 한국어와 중국어의 음소배열제약 대조 연구 = 韓中音素排列規則的對照硏究 / 상, 성남    원문복사 신청
   - 한·중 피동문 대조 연구 : 중국어 무표지 피동문과의 대조를 중심으로 = 韓中被動句對照硏究 : 以与中國語无標志被動句對照爲中心 / 신희경    원문복사 신청
   - 한중 고등학교 국어·어문(語⽂)과 교과서 비교 분석 연구 : 쓰기 영역을 중심으로
   - 한중 동물 속담의 문법 구조에 대한 대조 연구 : 12지지 동물의 속담을 중심으로 
   - 한중 사이버 대화 대조 연구 : 중국인 한국어 학습자의 인터넷상의 화용적 언어능력 향상을 위하여 = A contrastive study of Korea-China cyber dialogue - To improve the pragmatic competences of Chinese Korean learners on the Internet - / 부력엽 
   - 한중 사회호칭어에 대한 대조연구 : 드라마 대본을 중심으로 / Chi, Chenchen 
   - 한중 속담 대비 연구 : "용" 과 "호랑이" 에 관하여 = Korean-Chinese proverb contrast studies--With respect to the "Eastern Dragon" and "Tiger" / 유양    원문복사 신청
   - 한중 동물 속담의 문법 구조에 대한 대조 연구 : 12지지 동물의 속담을 중심으로 = 關於韓中動物俗語語法構造的對照?究:以12生肖爲中心 / JINHUIHUI    
   - 한중 언어문화 교육내용 대조 연구 / 조정순  
   - 현대 한중 한자 파생어 비교 연구 / 치전  
   - 한중 연어 사전 편찬을 위한 연구 / 왕유가
   - 한중 대조를 통한 완곡 표현 연구 / 채춘옥
   - 한중 신체어의 다의구조 대조연구 : '눈, 코, 입'의 의미를 중심으로 / 정수은
   - 한중 12간지 동물 사자성어 대조 연구 / 설서가  
   - 주어 생략의 한중 번역 연구 : 인터넷 신문을 중심으로 / 예흔이 
   - 한중 문학작품의 의성어·의태어 비교 연구 : 소설을 중심으로
   - ‘고양이’나 ‘쥐’가 포함되어 있는 한중(韓中) 속담 비교 연구 / SONG YAFANG 
   - 한중 동형 한자어의 의미 대조 연구 : - <한국어 학습용 어휘 목록>의 A등급 한자어를 중심으로 / 정동윤  
   - 한중(韓中) 중학교 국어·어문교과서의 '학습활동' 비교 연구
   - 한중 미각형용사 대조 연구 / 유연희  
   - 한중 학습 관련 동사 유의어 대조 연구 : 《설명결합사전》의 방법론을 중심으로 / 하완정 
   - 한중 동식물 관련 금기어 비교 연구 = A Comparison Study on the Taboo Language of Animals and Plants in China and Korea / 우상  
   - 한중 색채어 비교 연구 : 오색 계열 색채어를 중심으로 = 韩汉颜色词比较研究 / 권세라 
   - 한중 박쥐말의 차이 파악과 교육에 대한 연구 / 김지은
   - 한중 노래 번역 양상 고찰 : EXO곡을 중심으로 / Xu Ming
   - 한중 곤충 관련 관용 표현의 비교 연구 
   - 한중 피동문 대비와 학습 방안 연구 / 왕차오
   - 강수 현상과 관련된 한중 의태어 의미 대조 연구 / 那娜   
   - 한중 문학작품의 의성어·의태어 비교 연구 : 소설을 중심으
   - 한중 가족관계 속담의 문화특징 대조 연구 = A CONTRASTIVE STUDY ON THE CULTURAL FEATURES OF FAMILY RELATIONSHIPS PROVERBS IN KOREAN AND CHINESE / 고옥근 
   - 한중 여성 공손 발화 경계 억양 연구 = A study on the intonation phrase boundary tone of Korean-Chinese women polite speech / 가오쯔퉁 
   - ‘고양이’나 ‘쥐’가 포함되어 있는 한중(韓中) 속담 비교 연구 / SONG YAFANG   
   - 한중 동형 한자어의 의미 대조 연구 : - <한국어 학습용 어휘 목록>의 A등급 한자어를 중심으로 / 정동윤  
   - 한중(韓中) 중학교 국어·어문교과서의 '학습활동' 비교 연구 = A Comparative Study on ‘Learning Activities’ in Middle School Korean Language Textbooks in Korea and their Chinese Counterparts in China / 김지숙
   - 한중 미각형용사 대조 연구 / 유연희
   - 한중 어휘 대조를 통한 한국문화교육 방안 : 색채어를 중심으로 = A Study on the Educational Strategy of Korean Culture through Comparing Vocabulary between Korean and Chinese :the color words as the center / 유초
   - 한중 학습 관련 동사 유의어 대조 연구 : 《설명결합사전》의 방법론을 중심으로 / 하완정 
   - 한중 인터넷 신문 표제어 번역에 대한 연구 : 단문과 복문을 중심으로 = A Study of Translation of Headlines in Korean and Chinese Network News:centering on simple sentences and complex sentences / 가초남  
   - 한중 동사의 논항구조 대조 연구 : 한국어 위치말을 중심으로 = 韓漢動詞的論元結構對照研究: 以韓語位置語爲中心 / 공팅  
   - 한중 동식물 관련 금기어 비교 연구 = A Comparison Study on the Taboo Language of Animals and Plants in China and Korea / 우상 
   - 한중 색채어 비교 연구 : 오색 계열 색채어를 중심으로 = 韩汉颜色词比较研究 / 권세라  
   - 한중 시각 형용사의 의미 확장 양상에 대한 대조 연구 / 李天擇  
   - 한국어 ‘되다’ 구문표현의 중국어 대응 표현 연구 = 韓國語含有‘되다’的慣用組合韓中對照?究 / 주길    
   - 한중 친족호칭어 대비 연구 / 량솽  
   - 한중 신체 관련 관용어 비교 연구 : 눈을 중심으로 = A comparison study on the body idioms between Korean and Chinese : focus on eyes / 오양    원문복사 신청
   - 한중 주체 이동 동사의 의미 대조 연구 : 단의 분포 양상을 중심으로 / 백방 
   - 한중 박쥐말의 차이 파악과 교육에 대한 연구 / 김지은 
   - 한중 곤충 관련 관용 표현의 비교 연구 = A Comparison Study on the Idiomatical Expressions of Insects in Korean and Chinese / 조원   
   - 한중 피동문 대비와 학습 방안 연구 = A study on the teaching method of korean Passive sentence for chinese / 왕차오  
   - 한중 음식관련어 개념적 은유 대조 연구 / 양나영
   - 강수 현상과 관련된 한중 의태어 의미 대조 연구 / 那娜  
   - 한국어 교육을 위한 한중 신체어 관련 관용어 대조 연구 : '눈'과 '眼'을 중심으로 / He, Qing 
   - 한국어 조사 '의'의 중국어 대응양상 연구 : 한중 병렬 말뭉치 분석을 중심으로 / Shen, Yuhan 
   - 신문사설 코퍼스에 기반한 한중 번역 명시화 연구 : 접속기제를 중심으로 / 김혜림  
   - 한중 외래어의 형태·의미적 대조연구 / 담려려 
   - 한중 미각형용사 확장의미의 대조 연구 = The study of Korean and Chinese taste adjectives' extensive meaning / 원방려
   - 한중 미각형용사 대조 연구 / 이증  
   - 한중(韓中) 사동표현의 대조 연구 / 손영  
   - 한중 대우법의 대비 연구 / 자오쥔  
   - 한중 동의중복 표현 대조 연구 / ZHANG, JUNJUAN  
   - “개”와 관련된 한중 속담, 성어 대비연구 : . = WITH THE DOG RELATED PROVERBS AND IDIOMS COMPARATIVE STUDY BETWEEN KOREAN AND CHINESE / 임병  
   - 한중 피동문 대조 연구 : 중국어 被자문과의 대조를 중심으로 = 中韓被動句對照硏究:以對照被字句爲中心 / 양설  
   - 한중 부정극어 '아무'와 '任何‘의 대비연구 = A CONTRASTIVE STUDY OF NEGATIVE POLARITY ITEM BETWEEN KOREAN ‘A-MU’AND CHINESE ‘RENHE’ / 동립정
   - 한중 친족호칭어의 확대 사용 양상과 문화적 의미 대비 연구 = 韩中亲属称谓语的泛化现象及文化内涵对 比研究 / 장환
   - 한중 인터넷 금칙어의 비교 연구 = A Comparative Study on the Taboo on the Internet between Korean and Chinese / 손흔위
   - 한중 완곡어 대조 연구 / 종언선
   - 한중 미각 형용사 '시다' 계열 어휘의 대조 연구 = Comparative study of Korean and Chinese taste adjective '시다' / 주문형
   - 존재와 소유 동사에 대한 한중 대조 연구 / 쉬위제
   - 날씨 관련 은유 표현의 한중 대조 연구 = A comparative study of korean and chinese weather metaphors / 유군 
   - 중국인 한국어 학습자를 위한 한중 사자성어 대조 연구 : 한국어 교재와 한국어 능력시험(topik) 중심으로 = Comparative Analysis of 4-character Idiomatic Phrases in Korean and Chinese for the chinese to learn korean / 지아오쳔리
   - 한중 남녀 관련 비유 대조 연구 : 동물을 중심으로 / 곽걸  
   - 한중 ‘不’계 어휘 비교 연구 = Comparison Study Of '不‘ Expressions Between Korean and Chinese Language / 추육영  
   - 신체화에 기초한 한중 감각어의 의미 확장 연구 = Studies on the Meaning Extension of Sensory words based on the Embodiment in Korean and Chinese / 석수영 
   - 한중 이동동사 의미 대조 연구 : ‘들다’와 ‘?(j?n)/入(r?)’ 및 ‘나다’와 ‘出(ch?)’을 중심으로 = The Comparative Study of Korean and Chinese Movement Verbs’ Meaning: Focused on ‘deulda’ and ‘j?n/r?’, ‘nada’ and ‘ch?’ / 문아동 
   - 한국어 조사 {를}의 중국어 대응양상 연구 : 한중 중한 신문 병렬말뭉치 분석을 중심으로 / 임은정   
   - 한중 의성어와 의태어 대조 연구 / 王元媛 
   - 한중 번역에서 '머리'와 '头'의 어휘대응에 관한 연구 / 崔允貞 
   - 한중 부사어 어순 대비 연구 = A CONTRASTIVE STUDY ON THE ORDER OF ADVERBIALS IN KOREAN AND CHINESE / 범려나
   - 한중 중간언어의 동결식(动结式) 연구 / 유묘묘  
   - 한글을 통한 한중 한자어의 병행표기에 관한 연구 / 손연화 
   - '-것 같다'의 의미에 따른 한중 번역 연구 : 『아프니까 청춘이다』를 중심으로 / 이학지  
   - 한·중(韓中)인체어휘의 의미 확장 비교 연구 / 김보미 
   - 중국인 학습자를 위한 한중 동형 한자어의 의미대조 연구 / 사윤
   - 한중 높임법 대조 연구 : - 한국어 ‘님’의 용법을 중심으로 - / 서옥주 
   - 한중 동사 ‘내리다’와 ‘下’의 대조연구 = Comparing study of verbs ‘내리다’ in Korean and ‘下’ in Chinese / Shi Yang    원문복사 신청
   - 한중 통신 언어의 문법화와 역문법화 비교 연구 = A Comparative Study on the Grammaticalization and Degrammaticalization of Internet Language in Korea and China / 장부리
   - 한중 통각형용사 대조 분석 연구 : 아픔, 저림, 가려움에 관한 어휘를 중심으로 / TAN XIAORU 
   - 한중 접속문 대조 연구 : 논리 인과 접속문을 중심으로 / 송엽휘
   - 한중 담화표지 ‘아니’와 ‘不是’의 대조 연구 / 주자미  
   - 한중 '물'과 '불' 관련 관용어의 은유 의미 대조 연구 / Cao, Zhenna   
   - 중국어권 학습자를 위한 완곡 표현 대비 연구 : K-drama 자막 한중 번역본을 바탕으로 / 방이  
   - 100
   - 한중 계사구문의 유형론적 대조 연구 = ATypological-Contrastive Study on Copula Construction in Korean and Chinese / Tan, Na-na  
   - 한중 방향성 이동동사 구성의 대조 연구 = 韓·中方向性移動動詞結構的對照硏究 / 이, 남 
   - 한중 조건문의 대조 연구 = (A) contrastive study on conditional conjunctive sentence of Korean and Chinese / 이문용  
   - 한중 대립 접속 형태 '그러나'와 '但/但是' 대비 연구 / 최탁래 
   - 한중 신체 관련 관용어 비교 연구 = THE COMPARISON STADY ON THE BODY IDIOM BETWEEN KOREAN AND CHINESE / 김련화  
   - 한중 통증 표현 형용사 의미 대조 연구 / 왕야오 
   - 한중 경어법의 비교 연구 = A Comparative Study on Honorific System in Korean and Chinese Language / 손정원 
   - 한중 현대사회 호칭어의 대비연구 : 韩中现代社会称呼语的对比研究 = Research on the contrast between appellation of korean and china / 지려나    
   - 한중 관용어의 표현 양상과 내포 문화 비교 = A Comparative Study of the Expressional Patterns & Cultural Aspects between Korean and Chinese Idioms / 황정아 
   - 부사어의 한중 번역 양상에 관한 연구 / 차영란  
   - 현용하는 한중 동형이의어 대조 연구 / 강한식  
   - 동형의 한중 한자어 형용사 대비 연구 / 손해서 
   - 중국인 한국어 학습자를 위한 한중 속담 비교 연구 / 송효교   
   - 한.중 부정극어에 대한 대비 연구 = 韓中 否定极語的 對比硏究 / 향, 우우 
   - 한·중 동등 비교구문에 대한 인식 차이 연구 / 형몽결   
   - 한·중 ‘소(牛)’에 관한 속담 비교 연구 / 류설비
   - 한 · 중 피동 표현 대조 연구 : 피동문 유형 대조를 중심으로 = 韓中被動表現對照硏究 / 왕진곤
   - 중·한 중첩식 부사의 대응형식 연구 / 왕약옥
   - 한 중 정도부사 공기양상 대조 연구 / 가취명 
   - 한·중 미각 형용사 인지 의미 대조 연구 / 풍금란 
   - 한·중 수사의문문의 대조 연구 / 곽우 
   - 한·중 어순에 대한 유형론적 연구 : 명사구, 합성명사, 부치사구를 중심으로 / 손금추 
   - 병렬말뭉치 기반 한 중 사동표현의 대조 연구 : 유표지 사동을 중심으로 / 이문화  
   - 한ㆍ중 완곡에 의한 공손표현 대조 연구 / 蘇雨 
   - 한·중 반복어 대조분석 및 교육 방안 연구 / 허기  
   - 신체어 관용 표현의 한·중 통·번역 전략 / 방미원  
   - 말뭉치기반 한ㆍ중 분류사 대조 연구 / 유정정   
   - 한·중 인터넷 언어 비교연구 / 손철위  
   - 한국과 중국의 수사제도 비교 연구 / 박지성 
   - 한·중 사전의 동음어와 다의어에 대한 연구 : 동사를 중심으로 / 호흥휘 
   - 한·중·일 세 언어의 품사 대조 연구 : 명사, 동사, 형용사를 중심으로 = A Comparative Study on Parts of Speech in Korean, Chinese and Japanese / LIU WANYING 

140   -----

한중 한자어 분류사 통시적 대조연구

http://www.riss.kr/search/detail/DetailView.do?p_mat_type=be54d9b8bc7cdb09&control_no=4b79861397b12523ffe0bdc3ef48d419

  양사에 대한 비교 연구
  분류사를 = 양사
  동물에 대한 분류사의 대조   마리   

  언제시대에는 이렇게 쓰였는데 시대가 지나면서 이렇게 변했다.  발달이 어떻게 됬고..
  분류사 관련된 내용이 참고문헌..
    
    --------------------------------------------------------
    선행 연구 목적  부족하다 어떤식으로 쓰일건지 내용을 점 써라..
    -------------------------------








