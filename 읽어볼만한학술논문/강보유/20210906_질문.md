# 강보유 교수님 면담 질의 사항

안녕하세요 강보유 교수님
신소룡 교수님과 언어문화에 대한 연구를 진행하게된 ***입니다.
제가 진행하고자 하는 연구과제는 
한중 언어문화에 대한 연구로 
특히 한중 문화의 차이점과 유사성으로 인해 발생하게 되는 문법화의 양상에 대한 연구를 진행하려고 합니다.

세부 연구 방향성에 대한 것은 결정하지 못하였으며 아직 기초적인 조사 진행 단계 입니다.
아직 까지 언어 문화 및 언어학에 대한 지식이 적은 상태여서 혹시 제가 진행 하는 질문이 너무 기초 적일수도 있다면 양해 하여 주시기 바랍니다.

1. 아직 많은 조사를 해보지 않아서 인지 한국어 관련하여 언어 문화로 인해 발생 하게 되는 문법 요소에 대한 연구는 많지 않아 보였습니다.
그러던중 2017년 교수님의 논문 한국어_구문해석 방법론 에 대한 연구를 접하게 되었습니다.
한국어 문법 해석의 방향을 전환 해야 하며 구문구조 분석 보다는 왜 이런 구조로 짜였는가에 촛점을 맞추어 구문 해석을 진행 해야 하며
문화 인지적 해석기반을 마련 하셨으며
주관성과 주관화의 해석기반, 
원형과 범주화의 해석기반, 
어휘화와 문법화의 해석기반, 
자의성과 도상성의 해석기반 에 대한 언급을 하셨었습니다.

기반에 대한 언급이므로 관련하여 하위 연구 논문들이 많을것으로 기대 되는데. 혹시 발표된 논문이 존재 하는것인지?
그리고 만약 위의 각각의 해석기반과 관련 하여 어떤 소주제들을 만든다면 어떤형태가 적당하다고 생각하시는지?

ex)
주관성과 주관화의 해석기반
 - 내가 해라/ 네가 해라
 - ....

원형과 범주화의 해석기반
 - 나왔다 와 상
 - ...

어휘와 문법화의 해석기반
 - 


논문상 여기서 해석된 한국어 문법은 
 * 주관형용사 ~어하다 객관형용사 어지다
 * 일반적이고 객관적인 사실관계의 ~아서  주관적인 사실관계의 ~니까
   => 으니까는 과거의 추측이면서 더강한 주관성을 나타낸다.
 * 격조사의 주관화  
 * 범주화에 따라 명사에 대한 동사의 선택을 한국과 중국은 다르다.
 등의 해석된 문법이 존재 함을 확인 할수 있었습니다.

또한 동일 해석기반을 활용하여 해석된 중국어의 문법 요소들이 있는지?


해당 논문에서 언급하신 4개의 해석기반을 사용하여
한중의 문화 차이로 인해 서로 다른 양상으로 진행 되어진 문법요소들이란 기준으로 연구를 진행 한다고 할때
한중 문화의 의식 차이에 대한 부분이 중요해 보입니다.
예를들어 
한국은 생식문화와 이며 중국은 화식 문화 이다. 이것에 의해 한국어는 맛있다와 같은 객관적인 평가를 내리며
중국은 하오츠와 같은 주관적인 평가를 내린다.
한국어는 텔레미전 인터넷 추구장은 닫힌 공간인 집으로 인식한다면 한어는 열린공간인 무대로 인지하고 있다.
등등에 관련된 정리된 어떤것을 참조하면 좋을지?


원래는 나는 이문제에 대해서 이렇게 생각하는데 교수님의 생각은 어떤것인지를 물어봐야 하는데...
문제의 진입점에 대한 문의를 할수 밖에 없음...


















질문 1 
교수님의 논문 2017년 구문해석 방법론을 보면 문화의 영향을 받은 문법 요소
1. 






2. 강보유_201709_한국어_구문해석 방법론 에 보면 구문 해석 방법으로 제시된 
3. 